package com.sudomakmur.oopconcept

open class Manusia(nama : String){
    var nama : String? = ""
    init {
        this.nama = nama
    }

    open fun makan(){
        println("${this.nama} makan nasi ")
    }
}

class ManusiaMilenial(nama : String) : Manusia(nama) {
    var emailAttribut : String? = ""
    private var passwordAttribut : String? =""

    fun setEmail(emailParamater : String?){
        this.emailAttribut = emailParamater
    }

    fun setPassword(password : String?){
        this.passwordAttribut = password?.replace("a","*")
    }

    fun setPassword(password: String? , enkripsi : String){

    }

    fun getPassword() : String?{
        return this.passwordAttribut
    }

    override fun makan() {
        println("gasuka makan gamau gasuka gelay")
    }

    fun info(){
        println("${this.nama} mempunyai email dengan alamat : ${this.emailAttribut} dengan password : ${this.passwordAttribut} ")
    }
}



//abstract class Buah {
//    private var warna : String = ""
//
//    abstract fun caraMakan()
//
//    fun setWarna(warna : String){
//        this.warna = warna
//    }
//}


interface BuahInteface {

    fun pilihWarna(warna : String)
    fun JenisBuah(nama:String)
    fun UkuranBuah(ukuran:String)
}

class Durian : BuahInteface{

    override fun pilihWarna(warna: String) {
        println("Warna Buah Durian nya $warna")
    }

    override fun JenisBuah(nama: String) {
        println("Jenis Buah Durian nya $nama")
    }

    override fun UkuranBuah(ukuran: String) {
         println("Ukuran Buah Durian nya $ukuran")

    }

}


class AbangTukang {
    var buahInterface : BuahInteface

    init {
        buahInterface = Durian()
    }

    fun pilihWarna(value : String){
        buahInterface.pilihWarna(value)
    }
    fun namaBuah(value : String){
        buahInterface.JenisBuah(value)
    }
    fun UkuranBuah(value : String){
        buahInterface.UkuranBuah(value)
    }
}


fun main(){


    val abangTukang = AbangTukang()
    abangTukang.pilihWarna("Kuning")
    abangTukang.namaBuah("montonk")
    abangTukang.UkuranBuah("gede")

}